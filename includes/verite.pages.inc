<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Nico Heulsen
 * Date: 10/09/12
 * Time: 22:17
 */

function verite_overview_page() {
  $data = verite_load_all_timeline_blocks();

  $header = array(t('ID'), t('Title'), t('Source'), t('Width'), t('Height'), t('Operations'));
  $rows = array();

  foreach ((array)$data as $block) {
    $destination = drupal_get_destination();
    $operations = l(t('Edit'), 'admin/build/block/configure/verite/'. $block['delta'], array('query' => $destination));
    $operations .= ' | '. l(t('Delete'), 'admin/build/verite/delete/'. $block['id'], array('query' => $destination));

    $rows[] = array(
      'id' => $block['id'],
      'title' => $block['title'],
      'source' => $block['source'],
      'width' => $block['width'],
      'height' => $block['height'],
      'operations' => $operations,
    );
  }

  $output = '';
  $output .= '<p>'. l(t('Add verite block'), 'admin/build/block/verite') .'</p>';
  $output .= '<p>'. theme('table', $header, $rows) .'</p>';

  return $output;
}

/**
 * Delete a verite timeline block.
 *
 * @param $id
 */
function verite_delete_timeline_page($id) {
  verite_delete_timeline($id);

  //@todo: Add confirmation form
  $destination = !empty($_GET['destination']) ? $_GET['destination'] : 'admin/build/verite';
  drupal_goto($destination);
}
