<?php

  /**
  * Created by JetBrains PhpStorm.
  * User: Nico Heulsen
  * Date: 04/09/12
  * Time: 10:38
  *
  * $settings : Contains all block configuration settings.
  * $timeline_url : An url based on configuration settings to embed verite timeline.
  */

?>
<iframe src='<?php print $timeline_url; ?>' width='<?php print $settings['width']; ?>' height='<?php print $settings['height']; ?>' frameborder='0'></iframe>
