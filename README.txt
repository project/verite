Author:  Nico Heulsen
Released under the GPL


Description:
============
This module allows you to implement the VéritéCo Timeline.

Requirements:
=============
Drupal 6.x


Installation:
=============
Copy the entire library module directory to your module directory (sites/all/modules/contrib) and enable the verite module.


Verite Settings:
=========

Access Control:
===============
